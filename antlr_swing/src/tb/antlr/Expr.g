grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

// Notes:
// '!' - element will not be shown in the tree
// '?' - zero or once repetitions
// '*' - zero or more repetitions
// '+' - once or more repetitions

// '->' - rewrite rule for AST
// '^' -  use notation ^(...) for tree patterns not (...) in order to distinguish them from grammar subrules
// '^' - place symbol as a tree root

prog
    : PRINT? (stat)+ EOF!;


// '->' - regula przepisywania, '^' - znak modyfikujacy (to samo)
// expr NL (->) expr - Jezeli w strumieniu wejsciowym wykryto 'expr' i NL to wygeneruj drzewo, ktore zawiera tylko 'expr'
stat
    : expr NL -> expr

//    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)

    | NL ->
    ;


expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

PRINT: 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
