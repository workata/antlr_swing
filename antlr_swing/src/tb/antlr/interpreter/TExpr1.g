tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr {drukuj ($e.out.toString());})* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(PODST i1=ID   e2=expr) {
          symbol.newSymbol($i1.text);
          symbol.setSymbol($i1.text, $e2.out);
          $out = $e2.out;
        }
        | (PRINT e1=expr) {
          isPrintNeeded = true;
          $out = $e1.out;
        }
        | ID   {$out = symbol.getSymbol($ID.text);}
        | INT  {$out = getInt($INT.text);}
        ;
